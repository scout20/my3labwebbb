<!DOCTYPE html>
<html lang="ru">
   <head>
      <meta charset="utf-8">
      <title>Lab3</title>
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1">
   </head>
   <body>
      <div class="col-12 order-0 order-md-1">
         <h2 id="head3" style="text-align: center"><u>Форма</u></h2>
         <br>
         <form class="form-horizontal my_form" action="index.php" method="POST">
            <label> ФИО:
            <br>
            <input name="fio" value="Name" class="my_input">
            </label>
            <br>
            <p>
               <label> Email:
               <br>
               <input name="email" value="i@example.com" type="email" class="my_input">
               </label>
            </p>
            <p>
               <label> Дата рождения:
               <br>
               <input name="date" value="2020-03-15" type="date" class="my_input">
               </label>
            </p>
            <div class="radio">
               <label> Пол:
               <br>
               <input type="radio" name="gender" id="g1" value="Man"> Мужской
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="gender" id="g2" value="Woman"> Женский
               </label>
            </div>
            <br>
            <div class="radio">
               <label> Количество конечностей:
               <br>
               <input type="radio" name="kolvo" id="k1" value="1"> 1
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="kolvo" id="k2" value="2"> 2
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="kolvo" id="k3" value="3"> 3
               </label>
            </div>
            <div class="radio">
               <label>
               <input type="radio" name="kolvo" id="k4" value="4"> 4
               </label>
            </div>
            <br>
            <label>
               Сверхспособности:
               <br>
               <select name=superpower[] size=3 multiple="multiple">
                  <option value="Immortality">Бессмертие</option>
                  <option value="Passing through walls">Прохождение сквозь стены</option>
                  <option value="Levitation">Левитация</option>
               </select>
            </label>
            <br>
            <label> Биография:
            <br>
            <input type="text" name="bio" class="form-control form_text" value="Text">
            </label>
            <br>
            <div class="form-group">
               <div class="checkbox">
                  <label>
                  <input type="checkbox" name="contract"> С контрактом ознакомлен(а)
                  </label>
               </div>
            </div>
            <div class="form-group">
               <input type="submit" class="button" value="SEND">
            </div>
         </form>
      </div>
   </body>
</html>