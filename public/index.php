<?php
// Отправляем браузеру правильную кодировку.
// Файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некоторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например, метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_GET['save']))
    {
        // Если есть параметр save, то выводим сообщение пользователю.
        print ('Спасибо, данные сохранены!');
    }
    // Включаем содержимое файла form.php.
    include ('form.php');
    // Завершаем работу скрипта.
    exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
// Проверяем ошибки.
$errors = false;
if (empty($_POST['fio']))
{
    print ('Заполните имя.<br/>');
    $errors = true;
}
else if (!preg_match('/^[а-яА-Яa-zA-z ]+$/u', $_POST['fio']))
{
    print ('В имени присутствуют недопустимые символы.<br/>');
    $errors = true;
}
if (empty($_POST['email']))
{
    print ('Заполните почту.<br/>');
    $errors = true;
}
if (empty($_POST['date']))
{
    print ('Заполните дату рождения.<br/>');
    $errors = true;
}
if (!(isset($_POST['gender'])))
{
    print ('Выберите пол.<br/>');
    $errors = true;
}
if (!(isset($_POST['kolvo'])))
{
    print ('Выберите количество конечностей.<br/>');
    $errors = true;
}
if (empty($_POST['superpower']))
{
    print ('Выберите сверхспособности.<br/>');
    $errors = true;
}
if (empty($_POST['bio']))
{
    print ('Напишите биографию.<br/>');
    $errors = true;
}
if ($errors)
{
    // При наличии ошибок завершаем работу скрипта.
    exit();
}

// Сохранение в базу данных.
$user = 'u15645';
$pass = '3395578';
$db = new PDO('mysql:host=localhost;dbname=u15645', $user, $pass, array(
    PDO::ATTR_PERSISTENT => true
));

// Подготовленный запрос.
try
{
    $stmt = $db->prepare("INSERT INTO forma (name, email, date, gender, kolvo, superpower, bio, contract) VALUES (:name, :email, :date, :gender, :kolvo, :superpower, :bio, :contract)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':gender', $gender);
    $stmt->bindParam(':kolvo', $kolvo);
    $stmt->bindParam(':superpower', $superpower);
    $stmt->bindParam(':bio', $bio);
    $stmt->bindParam(':contract', $contract);
    $name = $_POST['fio'];
    $email = $_POST['email'];
    $date = $_POST['date'];
    $gender = $_POST['gender'];
    $kolvo = $_POST['kolvo'];
    

    // $superpower = "";
    // foreach ($_POST['superpower'] as $keys=>$values) {
    //     $superpower .= $values;
    //     $superpower .= '  ';
    // }
    
    
    $superpower = json_encode($_POST['superpower']);
    $bio = $_POST['bio'];
    $contract = $_POST['contract'];
    if ($contract == "on")
    {
        $contract = 1;
    }
    else
    {
        $contract = 0;
    }
    $stmt->execute();
}

catch(PDOException $e)
{
    print ('Error : ' . $e->getMessage());
    exit();
}
// stmt - это "дескриптор состояния".
// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.

header('Location: ?save=1');